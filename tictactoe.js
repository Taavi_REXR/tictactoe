var gridlength = parseInt(prompt("Please enter desired nr rows"));
var playerOneScore = 0;
var playerTwoScore = 0;
var playerOnePlacement = [];
var playerTwoPlacement = [];
var playersTurn = 0;
var dynamicCombinations = [];
var gamegrid = document.getElementById("gamegrid");
var playerOneNewPoints = document.getElementById("playerOneScore").innerHTML;
var playerTwoNewPoints = document.getElementById("playerTwoScore").innerHTML;
var playerOne = document.getElementById("playerOneScore");
var playerTwo = document.getElementById("playerTwoScore");

//The nr of one-way combinations is always (gridlength*2)+2
//Create combinations of rows. Run "gridlength" nr of times.
function rowCombinations() {
  for (rownr = 0; rownr < gridlength; rownr++) {
    var combination = [];
    //Create sequential combinations for rows
    for (boxnr = (rownr*gridlength + 1); boxnr < (rownr*gridlength+gridlength+1); boxnr++) {
      combination.push(boxnr)
    }
    dynamicCombinations.push(combination)
  }
}

//Create combinations of columns.
function columnCombinations() {
  for (colnr = 0; colnr < gridlength; colnr++) {
    var combination = [];
    var boxnr = colnr + 1;
    while ( boxnr < (gridlength*gridlength-(-colnr+gridlength)+2)) {
      combination.push(boxnr);
      boxnr = boxnr+gridlength;
    }
    dynamicCombinations.push(combination)
  }
}

//Create combinations for diagonals
function diagonalCombinations() {
  function topleftDiagaonal() {
    var combination = [];
    var boxnr = 1;
    while ( boxnr < (gridlength*gridlength+1)) {
      combination.push(boxnr);
      boxnr = boxnr+gridlength+1;
    }
    dynamicCombinations.push(combination)
  }
  topleftDiagaonal();
  function toprightDiagaonal() {
    var combination = [];
    var boxnr = gridlength;
    while ( boxnr < (gridlength*gridlength+(0-gridlength+1))+1) {
      combination.push(boxnr);
      boxnr = boxnr+gridlength-1;
    }
    dynamicCombinations.push(combination)
  }
  toprightDiagaonal();
}

function dynamicCombinationsPopulate() {
  rowCombinations();
  columnCombinations();
  diagonalCombinations();
}

//Pompare player's selections with possible winning combinations
function didWin() {
  var playerwon = false;
  var playerPlacements = [];

  (playersTurn == 0) ?
      (playerPlacements = playerOnePlacement)
    : (playerPlacements = playerTwoPlacement);

  if (playerPlacements.length >= gridlength) {
    for (let checkRun of dynamicCombinations) {
      var didFind = true;
      for (let elementId of checkRun) {
        var find = false;
        for (let element of playerPlacements) {
          if (elementId === element) {
            find = true;
            break
          }
        }
        if (!find) {
          didFind = false;
          break
        }
      }
      if(didFind) {
        playerwon = true;
        break
      }
    }
  }
  return playerwon;
}

function restartgame() {
  playersTurn = 0;
  playerOnePlacement = [];
  playerTwoPlacement = [];
}

//Restart game round at any time
document.getElementById("restartbutton").addEventListener("click", function(){
  restartgame();
  creategrid();
});

function winAndRestart() {
  alert("Player nr " + (playersTurn + 1) + " won!");
  (playersTurn == 0) ? playerOneNewPoints++ : playerTwoNewPoints++;
  playerOne.innerHTML = playerOneNewPoints;
  playerTwo.innerHTML = playerTwoNewPoints;
  restartgame();
  creategrid();
}

function drawAndRestart() {
  alert("Draw")
  restartgame();
  creategrid();
}

//Draw symbols, check for winning/draw condition
function playerMoves(e) {
  (playersTurn == 0) ?
  (this.innerHTML = "X", playerOnePlacement.push(parseInt(this.id))):
  (this.innerHTML = "O", playerTwoPlacement.push(parseInt(this.id)));

  didWin() ? winAndRestart()
    : (playerOnePlacement.length + playerTwoPlacement.length == (gridlength * gridlength)) ? drawAndRestart()
    : (playersTurn == 0) ? playersTurn = 1
    : playersTurn = 0;

  this.removeEventListener("click", arguments.callee);
};

//Create boxes with ids, delete old game field
function dynamicGameFieldPopulate() {
  var boxCounter = 1;
  while (gamegrid.hasChildNodes()) {
    gamegrid.removeChild(gamegrid.firstChild);
  }
  for (let rownr = 0; rownr < gridlength; rownr++) {
    let row = document.createElement("row");
    for (let boxnr = 0; boxnr < gridlength; boxnr++ ) {
      let box = document.createElement("box");
      box.id = boxCounter;
      box.addEventListener("click", playerMoves);
      row.appendChild(box);
      boxCounter++;
    }
    gamegrid.appendChild(row);
  }
}

//Create game field
function creategrid() {
  dynamicGameFieldPopulate();
  dynamicCombinationsPopulate();
}

creategrid();
